// Written by Heather Russell 
void print_truth_info() {

//to run: asetup AnalysisBase,21.2.55,here
// root -l $ROOTCOREDIR//scripts/load_packages.C print_truth_info.cxx
     std::auto_ptr< TFile > ifile(TFile::Open("/afs/path/to/file/AOD.15388521._000003.pool.root.1","READ"));

    TTree *t = (TTree*)ifile->Get("CollectionTree");

    xAOD::TEvent evt;
    evt.readFrom( ifile.get() );
    Long64_t nentries = t->GetEntries();
    std::cout<<"total events "<<nentries<<std::endl;

     for (int i = 0; i < nentries ; i++){  
     evt.getEntry(i);
     const xAOD::TruthParticleContainer* truthCont = 0;
     bool retrieve =  evt.retrieve (truthCont, "TruthParticles");

     for(int it=0; it< truthCont->size(); it++){
        const xAOD::TruthParticle* truth = truthCont->at(it);
    	if(fabs(truth->pdgId()) == 36){
    	   std::cout << "found particle A !" << " has nChildren: " << truth->nChildren()  << std::endl;
    	   for(int ic1 = 0; ic1 < truth->nChildren(); ic1++){
    	      const xAOD::TruthParticle* ch1 = truth->child(ic1);
    	      std::cout << " child: " << ch1->pdgId() << " has children: " << ch1->nChildren() << std::endl; 
    	   }
    	}
     }
   }
   return; //all ok
 
}